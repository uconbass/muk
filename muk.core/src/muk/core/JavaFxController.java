package muk.core;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import muk.core.command.CallbackableCommand;
import muk.core.command.Command;
import muk.core.command.CommandResult;
import muk.core.command.NullCommand;
import netscape.javascript.JSObject;


public class JavaFxController {
	public String pwd;

	public Stage stage;

	@FXML public TextField input;
	@FXML public WebView result;

	@FXML public void inputChanged(ActionEvent  event) {
		CommandList list = CommandList.getInstance();
		String text = input.getText();

		String cmd = CommandList.parseAlias(text);
		Command c = list.getCommand(cmd);
		if(! (c instanceof NullCommand)) {
			if(c instanceof CallbackableCommand) {
				((CallbackableCommand)c).initialize(result.getEngine(), input);
			}
			CommandResult r = c.execute(CommandList.parseArgs(text));
			if(r.isSuccess) {
				Properties.getInstance().putAll(r.properties);
				changeMessage(r.message);
				return;
			}
		}


		// TODO 使用可能コマンドの一覧表示コマンド
		if("help".equals(text)) {
			changeMessage(""
					+ "cd [dir] : 作業中ディレクトリの変更\n"
					+ "pwd : 作業中ディレクトリの確認\n"
					+ "exit : アプリケーション終了\n"
					+ "jseval : 数式を計算し、結果を表示する\n"
					+ "gs : Google検索\n"
					+ "grep : 作業中ディレクトリ直下のテキスト内容検索\n"
					+ "weather : 天気予報");
		}
	}

	public void setViewareaVisible(boolean visible) {
		result.setManaged(visible);
		stage.sizeToScene();
	}

	public void changeMessage(final Throwable t) {
		changeMessage(joinStackTrace(t));
	}

	public void changeMessage(final String message) {
		final WebEngine engine = result.getEngine();
		engine.load(JavaFxUI.class.getResource("/html.html").toExternalForm());
		engine.getLoadWorker().stateProperty().addListener(new ChangeListener<Object>() {
			@Override public void changed(
					ObservableValue<? extends Object> observable,
					Object oldValue,
					Object newValue)
			{
				JSObject o = (JSObject) engine.executeScript("document.getElementById('message')");
				o.setMember("innerHTML", message);
			}
		});

		setViewareaVisible(true);
	}


	public static String joinStackTrace(Throwable e) {
		StringBuilder sb = new StringBuilder();
		sb.append("[time]").append(System.currentTimeMillis()).append("\n");

		while(e != null) {
			sb.append(e).append("\n");

			for(StackTraceElement trace : e.getStackTrace()) {
				sb.append("\tat " + trace).append("\n");
			}

			e = e.getCause();
			if(e != null) {
				sb.append("Caused by:\n");
			}
		}
		return sb.toString();
	}
}
