package muk.core;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * アプリケーションのグローバル変数を管理するクラス
 */
public class Properties {
	public static String PWD = "pwd";


	private static final Properties instance = new Properties();
	private final ConcurrentMap<String, String> properties;

	private Properties() {
		properties = new ConcurrentHashMap<>();
	}

	public static Properties getInstance() {
		return Properties.instance;
	}

	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}

	public boolean containsValue(String value) {
		return properties.containsValue(value);
	}

	public String get(String key) {
		return properties.get(key);
	}

	public String put(String key, String value) {
		return properties.put(key, value);
	}

	public String remove(String key) {
		return properties.remove(key);
	}

	public void putAll(Map<String, String> m) {
		if(m == null || m.isEmpty()) return;
		properties.putAll(m);
	}

	public Set<String> keySet() {
		return properties.keySet();
	}

	public Collection<String> values() {
		return properties.values();
	}

	public Set<Map.Entry<String, String>> entrySet() {
		return properties.entrySet();
	}
}
