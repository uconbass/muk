package muk.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import muk.core.command.Command;
import muk.core.command.NullCommand;

public class CommandList {
	private static final CommandList instance = new CommandList();
	private final Set<Command> list = new HashSet<>();
	private final Map<String, Command> alias = new HashMap<>();

	private CommandList() {
	}

	public static CommandList getInstance() {
		return CommandList.instance;
	}

	public void addCommand(Command cmd) {
		if(cmd == null) throw new IllegalArgumentException("command is null.");
		addCommand(cmd.getCommandName(), cmd);
	}

	public void addCommand(String aliasName, Command cmd) {
		if(aliasName == null) throw new IllegalArgumentException("alias-name is null.");
		if(cmd == null) throw new IllegalArgumentException("command is null.");
		list.add(cmd);
		alias.put(aliasName, cmd);
	}

	public Command getCommand(String cmd) {
		Command c = alias.get(cmd);
		if(c == null) {
			c = NullCommand.get();
		}
		return c;
	}

	public static String parseAlias(String cmd) {
		if(cmd == null || cmd.isEmpty()) return null;

		String[] split = cmd.split(" ");
		return split[0];
	}

	public static String parseArgs(String cmd) {
		if(cmd == null || cmd.isEmpty()) return null;

		String[] split = cmd.split(" ", 2);
		return split.length == 2 ? split[1] : null;
	}
}
