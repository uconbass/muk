package muk.core;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;

public class HotKeyManager {
	public static final int OPEN = 1;

	private static final HotKeyManager instance = new HotKeyManager();
	private static final Map<Integer, HotKeyListener> hotKeyListener_ = new HashMap<>();

	private HotKeyManager() {
		// 使いづらいので、自前でJNI実装した方が良いかもしれない

		JIntellitype jIntelliType = null;
		try {
			jIntelliType = JIntellitype.getInstance();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		jIntelliType.registerHotKey(OPEN, JIntellitype.MOD_ALT, KeyEvent.VK_SPACE);
		jIntelliType.addHotKeyListener(new HotkeyListener() {
			@Override public void onHotKey(int hotKeyId) {
				HotKeyListener listener = hotKeyListener_.get(hotKeyId);
				if(listener != null) {
					listener.execute();
				}
			}
		});
	}

	public void addHotKeyListener(int hotKeyId, HotKeyListener listener) {
		hotKeyListener_.put(hotKeyId, listener);
	}

	/**
	 * アプリケーション終了時に必ず実行しなければならない
	 */
	public static void creanUp() {
		JIntellitype jIntelliType = JIntellitype.getInstance();
		jIntelliType.unregisterHotKey(OPEN);
		jIntelliType.cleanUp();
	}

	public static HotKeyManager getInstance() {
		return HotKeyManager.instance;
	}
}
