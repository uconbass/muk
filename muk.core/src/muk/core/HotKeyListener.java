package muk.core;

/**
 * ホットキーが入力された時に実行されるListener
 */
public interface HotKeyListener {
	void execute();
}
