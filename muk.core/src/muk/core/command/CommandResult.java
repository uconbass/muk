package muk.core.command;

import java.util.Map;

public class CommandResult {
	public boolean isSuccess;
	public String message;
	public Map<String, String> properties;	// TODO グローバル変数の変更方法を考える必要がある

	public static CommandResult success(String message) {
		return createCommandResult(true, message);
	}

	public static CommandResult fail(String message) {
		return createCommandResult(false, message);
	}

	private static CommandResult createCommandResult(boolean isSuccess, String message) {
		CommandResult result = new CommandResult();
		result.isSuccess = isSuccess;
		result.message = message;
		return result;
	}
}
