package muk.core.command;

import muk.core.GrepSearch;
import muk.core.Properties;

public class Grep implements Command {

	@Override public String getCommandName() {
		return "grep";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result;
		try {
			String message  =GrepSearch.grep(Properties.getInstance().get(Properties.PWD), args.trim());
			result = CommandResult.success(message);

		} catch (Exception e) {
			result = CommandResult.fail(e.getMessage());
		}
		return result;
	}

}
