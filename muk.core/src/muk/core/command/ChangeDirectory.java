package muk.core.command;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import muk.core.Properties;


/**
 * 作業ディレクトリを変更する
 */
public class ChangeDirectory implements Command {

	@Override public String getCommandName() {
		return "cd";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result = null;
		File f = new File(args);
		if(f.isDirectory()) {
			String dir = f.getAbsolutePath();

			result = CommandResult.success(dir);
			Map<String, String> properties = new HashMap<>();
			properties.put(Properties.PWD, dir);
			result.properties = properties;
		} else {
			result = CommandResult.fail(args + " is not directory.");
		}

		return result;
	}
}
