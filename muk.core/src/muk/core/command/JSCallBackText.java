package muk.core.command;


public class JSCallBackText extends CallbackableCommand {

	@Override public String getCommandName() {
		return "test";
	}

	@Override
	public CommandResult executeInner(String args) {
		return CommandResult.success(message);
	}

String message = ""
		+ "<a href=\"#\" onclick=\"app.sample('1')\">1</a><br />"
		+ "<a href=\"#\" onclick=\"app.sample(2, 3, 4)\">2</a><br />"
		+ "<a href=\"#\" onclick=\"app.sample(3)\">3</a><br />"
		+ "<a href=\"#\" onclick=\"app.sample(4)\">4</a><br />";


	public void sample(Object o) {
		if(o == null) {
			System.out.print("NULL");
		}

		System.out.println(o + " " + o.getClass());
		setCommand(o.toString());
	}
}
