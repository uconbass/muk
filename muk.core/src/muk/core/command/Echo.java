package muk.core.command;


/**
 * 入力されたものをそのまま返す
 */
public class Echo implements Command {

	@Override public String getCommandName() {
		return "echo";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result = new CommandResult();
		result.isSuccess = true;
		result.message = args;
		return result;
	}
}
