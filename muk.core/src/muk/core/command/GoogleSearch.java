package muk.core.command;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

public class GoogleSearch implements Command {

	@Override public String getCommandName() {
		return "gs";
	}

	@Override public CommandResult execute(String args) {
		try {
			String query = URLEncoder.encode(args, "utf-8");
			Desktop desktop = Desktop.getDesktop();
			URI uri = new URI("http://google.com/search?q=" + query);
			desktop.browse(uri);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
		return CommandResult.success("");
	}

}
