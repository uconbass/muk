package muk.core.command;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class JSEval implements Command {
	private static final ScriptEngine e = new ScriptEngineManager().getEngineByName("js");

	@Override public String getCommandName() {
		return "jseval";
	}

	@Override
	public CommandResult execute(String args) {
		CommandResult result = null;
		try {
			String message = eval(args);
			result = CommandResult.success(message);
		} catch(ScriptException e) {
			result = CommandResult.fail(e.getMessage());
		}

		return result;
	}

	/**
	 * expression を JS の eval() で処理する
	 * @param expression
	 * @return JSで処理した結果
	 * @throws ScriptException
	 */
	public static String eval(String expression) throws ScriptException {
		return e.eval(expression).toString();
	}
}
