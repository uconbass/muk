package muk.core.command;

import javafx.application.Platform;

/**
 * アプリケーションの終了
 */
public class Exit implements Command {

	@Override public String getCommandName() {
		return "exit";
	}

	@Override public CommandResult execute(String args) {
		Platform.exit();

		CommandResult result = new CommandResult();
		result.isSuccess = true;
		return result;
	}

}
