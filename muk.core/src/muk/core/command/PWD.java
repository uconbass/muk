package muk.core.command;

import muk.core.Properties;


/**
 * 作業ディレクトリを表示する
 */
public class PWD implements Command {

	@Override public String getCommandName() {
		return "pwd";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result = new CommandResult();
		result.isSuccess = true;
		result.message = Properties.getInstance().get(Properties.PWD);
		return result;
	}
}
