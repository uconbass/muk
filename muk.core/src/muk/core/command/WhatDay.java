package muk.core.command;

import info.bliki.wiki.model.WikiModel;

import java.util.Calendar;

import muk.core.Wiki;

public class WhatDay implements Command {
	public static String get() {
		try {
			Calendar c = Calendar.getInstance();
			Wiki w = new Wiki("ja.wikipedia.org");
			String str = w.getPageText((c.get(Calendar.MONTH) + 1) + "月" + c.get(Calendar.DAY_OF_MONTH) + "日");
			return WikiModel.toHtml(str);
		} catch(Exception e) {
			e.printStackTrace();
			return "error.";
		}
	}

	@Override public String getCommandName() {
		return "what-day";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result = CommandResult.success(get());
		return result;
	}
}
