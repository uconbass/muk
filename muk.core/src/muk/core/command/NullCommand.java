package muk.core.command;


/**
 * Null Command
 */
public class NullCommand implements Command {
	public static Command NULL_COMMAND = new NullCommand();

	private NullCommand() {
	}


	@Override public String getCommandName() {
		return null;
	}

	@Override public CommandResult execute(String command) {
		CommandResult result = new CommandResult();
		result.isSuccess = true;
		return result;
	}

	public static Command get() {
		return NULL_COMMAND;
	}
}
