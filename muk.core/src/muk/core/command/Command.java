package muk.core.command;

public interface Command {
	public String getCommandName();
	public CommandResult execute(String args);
}
