package muk.core.command;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class Twitter implements Command {
	private twitter4j.Twitter twitter = TwitterFactory.getSingleton();

	@Override public String getCommandName() {
		return "tw";
	}

	@Override public CommandResult execute(String args) {
		try {
			AccessToken token = twitter.getOAuthAccessToken();
			Status status = twitter.updateStatus(args);
			System.out.println("Successfully updated the status to [" + status.getText() + "].");
			return CommandResult.success("Successfully updated the status to [" + status.getText() + "].");
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return CommandResult.fail("失敗");
	}

}
