package muk.core.command;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import net.arnx.jsonic.JSON;

public class Weather implements Command {
	public static String WEATHER_API = "http://weather.livedoor.com/forecast/webservice/json/v1?city={city-id}";
	private static String DEFAULT_CITY_ID = "130010";

	public static String get() {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(WEATHER_API.replace("{city-id}", DEFAULT_CITY_ID));
			conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("GET");
			conn.addRequestProperty("Accept-Charset", "utf-8");
			conn.connect();

			Map<?, ?> resBody = (Map<?, ?>) ((Map<?, ?>) JSON.decode(conn.getInputStream()));
			conn.disconnect();

			String time = (String) resBody.get("publicTime");
			String title = (String) resBody.get("title");
			String description = (String) ((Map<?, ?>) resBody.get("description")).get("text");

			StringBuilder sb = new StringBuilder();
			sb.append(title).append("\n");
			sb.append(description).append("\n");
			sb.append(time);
			return sb.toString();
		} catch(Exception e) {
			return "error.";
		} finally {
			if(conn != null) {
				conn.disconnect();
			}
		}
	}

	@Override public String getCommandName() {
		return "weather";
	}

	@Override public CommandResult execute(String args) {
		CommandResult result = CommandResult.success(get());
		return result;
	}
}
