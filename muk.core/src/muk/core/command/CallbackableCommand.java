package muk.core.command;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import netscape.javascript.JSObject;

public abstract class CallbackableCommand implements Command {
	public WebEngine engine;
	public TextField input;

	private boolean isInitialized = false;

	@Override public final CommandResult execute(String args) {
		if(! isInitialized) {
			throw new IllegalStateException("初期化されていないコマンドです.");
		}

		return executeInner(args);
	}

	protected abstract CommandResult executeInner(String args);

	public void initialize(final WebEngine engine, TextField input) {
		if(isInitialized) {
			return;
		}

		this.engine = engine;
		this.input = input;

		// コールバックを受け取るクラスを設定する
		engine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
			@Override public void changed(ObservableValue<? extends State> ov, State t, State t1) {
				if (t1 == Worker.State.SUCCEEDED) {
					JSObject window = (JSObject) engine.executeScript("window");
					window.setMember("app", CallbackableCommand.this);
				}
			}
		});

		isInitialized = true;
	}

	protected WebEngine getWebEngine() {
		return this.engine;
	}

	protected void setCommand(String cmd) {
		this.input.setText(cmd);
	}
}
