package muk.core;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import muk.core.command.ChangeDirectory;
import muk.core.command.Echo;
import muk.core.command.Exit;
import muk.core.command.GoogleSearch;
import muk.core.command.Grep;
import muk.core.command.JSCallBackText;
import muk.core.command.JSEval;
import muk.core.command.PWD;
import muk.core.command.Twitter;
import muk.core.command.Weather;
import muk.core.command.WhatDay;

public class JavaFxUI extends Application {
	public static void main(String[] args) throws Exception {
		System.out.println("start");


		try {
		System.out.println("launch start");
		Application.launch(args);
		} catch(Exception e) {
			System.out.println(e);
			e.printStackTrace(System.out);
		}
	}

	@Override public void start(final Stage stage) throws Exception {
		CommandList list = CommandList.getInstance();
		list.addCommand(new Exit());
		list.addCommand(new Echo());
		list.addCommand(new ChangeDirectory());
		list.addCommand(new PWD());

		list.addCommand(new JSEval());
		list.addCommand(new Weather());
		list.addCommand(new GoogleSearch());
		list.addCommand(new Grep());

		list.addCommand(new WhatDay());

		list.addCommand(new JSCallBackText());

		list.addCommand(new Twitter());

		/*
		 * 自動でアプリケーションが終了することを禁止する
		 * Platform.setImplicitExit(false) を設定しない場合は、
		 * ウィンドウが閉じる(stage.hide()) と同時にアプリケーションが終了する
		 */
		Platform.setImplicitExit(false);

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/JavaFxUI.fxml"));
		loader.load();
		Parent root = loader.<Parent>getRoot();
		final JavaFxController controller = loader.<JavaFxController>getController();
		controller.stage = stage;
		controller.setViewareaVisible(false);

		Scene scene = new Scene(root);
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.setScene(scene);

		// 表示
		stage.show();

		HotKeyManager.getInstance().addHotKeyListener(HotKeyManager.OPEN,
				new HotKeyListener() {
					@Override
					public void execute() {
						Platform.runLater(new Runnable() {
							@Override public void run() {
								if (stage.isShowing()) {
									stage.hide();
								} else {
									controller.setViewareaVisible(false);
									stage.show();
								}
							}
						});

					}
				});
	}

	@Override public void stop() throws Exception {
		HotKeyManager.creanUp();
		super.stop();
	}
}
