package muk.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.ja.JapaneseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class GrepSearch {

	public static String grep(String targetDir, String query) throws IOException {
		File index = new File("./index");
        index(new File(targetDir), index); // インデックス
		return query(index, query); // 検索
	}

	public static void main(String[] args) throws Exception {
		// 全文検索をしたいファイルを格納するフォルダを指定します
		File fileDir = new File("index");

		// インデックスを格納するフォルダ
		File indexDir = new File("index");

		index(fileDir, indexDir); // インデックス
		query(indexDir, "query string"); // 検索
	}

	private static void index(File fileDir, File indexDir) throws IOException {

		// インデックス作成
		Analyzer luceneAnalyzer = new JapaneseAnalyzer();
		IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexDir),
				new IndexWriterConfig(Version.LUCENE_4_10_1, luceneAnalyzer));

		File[] textFiles = fileDir.listFiles();
		long startTime = new Date().getTime();

		// ファイルごとのインデックス作成
		for (int i = 0; i < textFiles.length; i++) {
			if (textFiles[i].isFile()
					&& textFiles[i].getName().endsWith(".txt")) {
				System.out.println(" ファイル：  " + textFiles[i].getCanonicalPath()
						+ " インデックス中... ");

				indexFileContent(indexWriter, textFiles[i].getCanonicalPath(),
						"utf-8");
			}
		}
		// indexWriter.commit();
		indexWriter.close();

		// インデックス時間をログ
		long endTime = new Date().getTime();
		System.out.println("インデックス時間：" + (endTime - startTime));
	}

	static void addDoc(IndexWriter writer, Document doc, String key,
			String value) throws IOException {
		doc.add(new Field(key, value, Store.YES, Index.ANALYZED));
		writer.addDocument(doc);
	}

	// ファイル内容を行ごとにインデックスに追加
	private static void indexFileContent(IndexWriter writer, String fileName,
			String charset) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(fileName), charset));
		String line = new String();

		int lineNo = 0;
		while ((line = reader.readLine()) != null) {
			Document doc = new Document();
			addDoc(writer, doc, "FilePath", fileName);
			lineNo++;
			addDoc(writer, doc, "FileContent", lineNo + "行目：" + line);
		}
		reader.close();
	}

	// 検索してみる
	private static String query(File indexDir, String queryString)
			throws IOException {
		StringBuilder sb = new StringBuilder();


		Query query = null;

		long startTime = new Date().getTime();

		// indexフォルダ

		IndexReader reader = DirectoryReader.open(FSDirectory.open(indexDir));
		IndexSearcher searcher = new IndexSearcher(reader);

		Analyzer analyzer = new JapaneseAnalyzer();
		try {
			// FileContent から検索
			QueryParser qp = new QueryParser("FileContent", analyzer);
			query = qp.parse(queryString);
		} catch (ParseException e) {
		}
		if (searcher != null) {
			TopDocs topdocs = searcher.search(query, 10000);
			if (topdocs.totalHits > 0) {
				sb.append("検索結果：" + topdocs.totalHits + " 件\n");
				for (ScoreDoc scoreDoc : topdocs.scoreDocs) {

					Document doc = searcher.doc(scoreDoc.doc);

					sb.append(doc.get("FilePath")).append("\n");
					sb.append(doc.get("FileContent")).append("\n");
				}
			}
		}

		long endTime = new Date().getTime();
		sb.append("検索時間：" + (endTime - startTime) + "ms.");
		return sb.toString();
	}
}
